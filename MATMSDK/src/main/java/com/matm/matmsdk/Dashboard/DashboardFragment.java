package com.matm.matmsdk.Dashboard;


import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import com.matm.matmsdk.Interface.Dashboard.DashboardApiInterface;
import com.matm.matmsdk.MPOS.PosActivity;
import com.matm.matmsdk.Model.Dashboard.Dashboard;
import isumatm.androidsdk.equitas.R;
import com.matm.matmsdk.Service.ApiAuthFactory;
import com.matm.matmsdk.UserProfile.UserProfileActivity;
import com.matm.matmsdk.Utils.EnvData;
import com.matm.matmsdk.Utils.ResultEvent;
import com.paxsz.easylink.api.EasyLinkSdkManager;
import com.paxsz.easylink.api.ResponseCode;
import com.paxsz.easylink.device.DeviceInfo;
import org.greenrobot.eventbus.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment implements PopupMenu.OnMenuItemClickListener{

    private TextView userName;
    private TextView userFullName;
    private TextView userBalance;
    private ImageView startMpos;
    private ImageView profileEdit;
    private EasyLinkSdkManager manager;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View dashboardView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        manager = EasyLinkSdkManager.getInstance(getActivity());

        ImageView top_menu = dashboardView.findViewById(R.id.top_menu);
        top_menu.setOnClickListener(view -> {
            PopupMenu popup = new PopupMenu(getContext(), view);
            popup.setGravity(Gravity.END);
            popup.setOnMenuItemClickListener(DashboardFragment.this::onMenuItemClick);
            popup.inflate(R.menu.top_menu_popup);
            popup.show();
        });


        userName = dashboardView.findViewById(R.id.userName);
        userFullName = dashboardView.findViewById(R.id.userFullName);
        userBalance = dashboardView.findViewById(R.id.userBalance);
        startMpos = dashboardView.findViewById(R.id.startMpos);
        profileEdit = dashboardView.findViewById(R.id.profileEdit);

        DashboardApiInterface dashboardApiService = ApiAuthFactory.getClient().create(DashboardApiInterface.class);
        Call<Dashboard> call = dashboardApiService.getDashboard();
        call.enqueue(new Callback<Dashboard>() {
            @Override
            public void onResponse(Call<Dashboard> call, Response<Dashboard> response) {
                // Log.d("Success", String.valueOf(response.body()));
                if(response.isSuccessful()){
                    Dashboard dashboardResponse = response.body();
//                Toast.makeText(getContext(), dashboardResponse.getUserInfo().getUserName(), Toast.LENGTH_SHORT).show();
                    userName.setText("@" + dashboardResponse.getUserInfo().getUserName());
                    userFullName.setText(dashboardResponse.getUserInfo().getUserProfile().getFirstName() + " " + dashboardResponse.getUserInfo().getUserProfile().getLastName());
                    userBalance.setText("\u20B9 " + dashboardResponse.getUserInfo().getUserBalance());
                    EnvData.UserName = dashboardResponse.getUserInfo().getUserName();
                    EnvData.AdminName = dashboardResponse.getUserInfo().getAdminName();
                    EnvData.UserBalance = dashboardResponse.getUserInfo().getUserBalance();
                    EnvData.UserType = dashboardResponse.getUserInfo().getUserType();
                    EnvData.userProfile = dashboardResponse.getUserInfo().getUserProfile();
                    new Thread(runnable).start();
                }else{
                    // Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();

                }


            }

            @Override
            public void onFailure(Call<Dashboard> call, Throwable t) {
                call.cancel();
                Log.d("Failure", String.valueOf(t));
            }
        });

        startMpos.setOnClickListener(v -> {
            Intent posIntent = new Intent(getContext(), PosActivity.class);
            startActivity(posIntent);
        });

        profileEdit.setOnClickListener(view -> {
            Intent sharedActivity = new Intent(getContext(), UserProfileActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Pair[] pairs = new Pair[3];
                pairs[0] = new Pair(profileEdit,profileEdit.getTransitionName());
                pairs[1] = new Pair(userName,userName.getTransitionName());
                pairs[2] = new Pair(userFullName,userFullName.getTransitionName());
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(),pairs);

                startActivity(sharedActivity,options.toBundle());
            }
            else {
                startActivity(sharedActivity);
            }

        });


        // Inflate the layout for this fragment
        return dashboardView;
    }
    Runnable runnable = new Runnable()
    {
        @Override
        public void run()
        {
            checkDeviceDeviceInfo();
        }
    };
    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
//        Toast.makeText(getContext(), "Selected Item: " + menuItem.getTitle(), Toast.LENGTH_SHORT).show();
        int itemId = menuItem.getItemId();
        if (itemId == R.id.set_mpin) {// do your code
            return true;
        } else if (itemId == R.id.profile) {
            Intent sharedActivity = new Intent(getContext(), UserProfileActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Pair[] pairs = new Pair[3];
                pairs[0] = new Pair(profileEdit, profileEdit.getTransitionName());
                pairs[1] = new Pair(userName, userName.getTransitionName());
                pairs[2] = new Pair(userFullName, userFullName.getTransitionName());
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), pairs);

                startActivity(sharedActivity, options.toBundle());
            } else {
                startActivity(sharedActivity);
            }
            return true;
        } else if (itemId == R.id.change_password) {// do your code
            return true;
        } else if (itemId == R.id.logout) {
            logout();
            return true;
        }
        return false;
    }

    private void logout(){
       // LoginSession.clearData(getContext());
       // Intent loginIntent = new Intent(getContext(), LoginActivity.class);
       // startActivity(loginIntent);
    }

    private void checkDeviceDeviceInfo(){
        SharedPreferences pref = getActivity().getSharedPreferences("AuthData", 0);
        if(pref.contains("DEVICE_NAME") && pref.contains("DEVICE_MAC")){
            connectDevice(pref.getString("DEVICE_NAME",""),pref.getString("DEVICE_MAC",""));
        }
    }

    public void doEvent(ResultEvent event) {
        EventBus.getDefault().post(event);

    }

    private void connectDevice(String deviceName, String deviceMac) {
        DeviceInfo deviceInfo = new DeviceInfo(DeviceInfo.CommType.BLUETOOTH, deviceName, deviceMac);
        int ret = manager.connect(deviceInfo);
        /*if (getActivity().isDestroyed()) {
            return;
        }*/
        if (ret == ResponseCode.EL_RET_OK) {
            Log.i("log", "connect success");
            doEvent(new ResultEvent(ResultEvent.Status.CONNECT_SUCCESS));
            System.out.println(">>>>-----  "+deviceInfo.getIdentifier());
            Log.i("log", deviceInfo.getIdentifier());

        } else {
            doEvent(new ResultEvent(ResultEvent.Status.CONNECT_FAILED, ret));
        }
    }
}
